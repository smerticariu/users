import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Button from 'material-ui/Button';
import Toolbar from 'material-ui/Toolbar';

import { AppBar } from 'material-ui';
import Hello from './Hello';
import Users from './Users';

class Navbar extends Component {
  render() {
    return (
      <div>
        <Router>
          <div>
            <AppBar title="Title">
              <Toolbar>
                <div>
                  <Link className="link" to="/">
                    <Button raised color="primary">
                      Home
                    </Button>
                  </Link>

                  <Link className="link" to="/users">
                    <Button raised color="primary">
                      Users
                    </Button>
                  </Link>
                </div>
              </Toolbar>
            </AppBar>
            <Route exact path="/" component={Hello} />
            <Route path="/users" component={Users} />
          </div>
        </Router>
      </div>
    );
  }
}
export default Navbar;
