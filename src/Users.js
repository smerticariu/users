import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

// Material design
import Button from 'material-ui/Button';
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';

import Modal from 'material-ui/Modal';

import TextField from 'material-ui/TextField';

function getModalStyle() {
  return {
    top: `40%`,
    left: `40%`,
    position: 'absolute',
    backgroundColor: '#fff',
    width: '400px'
  };
}

const api_url = 'http://localhost:3000';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      users: [],
      name: '',
      username: '',
      userToEdit: {}
    };
    this._addUser = this._addUser.bind(this);
    this._onChange = this._onChange.bind(this);
    this._resetFields = this._resetFields.bind(this);
    this._displayUsers = this._displayUsers.bind(this);
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    this._getUsers();
  }

  _getUsers() {
    axios
      .get(`${api_url}/data`)
      .then(response => {
        this.setState({
          users: response.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  _addUser(e) {
    e.preventDefault();
    const { name, username } = this.state;

    axios
      .post(`${api_url}/data`, { name, username })
      .then(response => {
        console.log(response);
        this._getUsers();
      })
      .catch(err => {
        console.log(err);
      });
    this._resetFields();
  }

  _removeUser(id) {
    axios
      .delete(`${api_url}/data/${id}`)
      .then(response => {
        console.log(this, 'users state');
        this._getUsers();
      })
      .catch(err => {
        console.log(err);
      });
  }

  _getUser(id) {
    axios
      .get(`${api_url}/data/${id}`)
      .then(response => {
        this.setState({ userToEdit: response.data });
        this.handleOpen();
      })
      .catch(err => {
        console.log(err);
      });
  }

  _editUser(id) {
    let userToEdit = Object.assign({}, this.state.userToEdit);
    userToEdit.name = this.refs['edit-form'].name.value;
    userToEdit.username = this.refs['edit-form'].username.value;

    this.setState({ userToEdit }, () => {
      axios
        .put(`${api_url}/data/${id}`, this.state.userToEdit)
        .then(response => {
          this._getUsers();
        })
        .catch(err => {
          console.log(err);
        });
    });
  }

  _displayUsers() {
    const { users, userToEdit } = this.state;
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Username</TableCell>
              <TableCell>Edit</TableCell>
              <TableCell>Delete</TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {users.map(user => {
              return (
                <TableRow key={user.id}>
                  <TableCell>{user.name}</TableCell>
                  <TableCell>{user.username}</TableCell>
                  <TableCell>
                    <Button
                      onClick={() => {
                        this._getUser(user.id);
                      }}
                    >
                      Edit
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Button
                      raised
                      color="secondary"
                      onClick={() => {
                        this._removeUser(user.id);
                      }}
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        <Modal open={this.state.open} onClose={this.handleClose}>
          <div style={getModalStyle()}>
            <Paper>
              <form
                ref="edit-form"
                onSubmit={e => {
                  e.preventDefault();
                  this._editUser(userToEdit.id);
                }}
              >
                <TextField
                  label="Name"
                  defaultValue={userToEdit.name}
                  onChange={this._onChange}
                  name="name"
                  helperText="Please enter the name"
                  margin="dense"
                  required
                />
                <TextField
                  label="Dense"
                  id="margin-dense"
                  defaultValue={userToEdit.username}
                  name="username"
                  onChange={this._onChange}
                  helperText="Please enter the username"
                  margin="dense"
                  required
                />

                <Paper>
                  <Button raised color="primary" type="submit">
                    Submit
                  </Button>
                  <Button raised color="primary" onClick={this.handleClose}>
                    Cancel
                  </Button>
                </Paper>
              </form>
            </Paper>
          </div>
        </Modal>
      </Paper>
    );
  }

  _resetFields() {
    this.refs.form.name.value = null;
    this.refs.form.username.value = null;
    this.setState({
      name: '',
      username: ''
    });
  }

  _onChange = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  render() {
    const { name, username } = this.state;

    return (
      <div>
        {this._displayUsers()}

        <Paper>
          <form ref="form" onSubmit={this._addUser}>
            <TextField
              label="Name"
              defaultValue={name}
              onChange={this._onChange}
              name="name"
              helperText="Please enter the name"
              required
            />
            <TextField
              label="Dense"
              id="margin-dense"
              defaultValue={username}
              name="username"
              onChange={this._onChange}
              helperText="Please enter the username"
              margin="dense"
              required
            />

            <Button raised color="primary" type="submit">
              Submit
            </Button>
          </form>
        </Paper>
      </div>
    );
  }
}

export default App;
