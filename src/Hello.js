import React, { Component } from 'react';
import logo from './logo.svg';
class Hello extends Component {
  render() {
    return (
      <div>
        <img className="logo" alt="logo" src={logo} />
      </div>
    );
  }
}

export default Hello;
